## REST APIs with Spring Boot, H2, JPA and Hibernate for managing users

### Library and tools

The app uses following tools and libs.

    gradle
    
    jdk 17
    
    lombok
    
    mapstruct

### Steps to Setup

**1. Clone the application**

```bash
https://gitlab.com/tvm3/user
```

**2. Build and run the app using gradle**

```bash
gradlew bootRun
```

The app will start running at <http://localhost:8080>.

### Explore Rest APIs

The app defines following CRUD APIs.

    GET /users
    
    POST /users
    
    GET /users/{id}
    
    PUT /users
    
    DELETE /users/{userId}
