package com.tvm.user.controller;

import com.tvm.user.model.User;
import com.tvm.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok(this.userService.getUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable final long id) {
        return ResponseEntity.ok(this.userService.getUser(id));
    }

    @PutMapping
    public ResponseEntity<User> updateUser(@RequestBody final User user) {
        return ResponseEntity.ok(this.userService.updateUser(user));
    }

    @PostMapping
    public ResponseEntity<User> addUser(@RequestBody final User user) {
        return ResponseEntity.ok(this.userService.addUser(user));
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable final long id) {
        this.userService.deleteUser(id);
    }
}
