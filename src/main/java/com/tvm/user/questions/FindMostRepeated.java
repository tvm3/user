package com.tvm.user.questions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FindMostRepeated {
    public static void main(final String[] args) {
        FindMostRepeated.find(Arrays.asList("apple", "apple", "apple", "apple", "apple", "pie", "apple", "red", "red", "red"));
        //FindMostRepeated.find(Arrays.asList("apple", "pie", "apple", "red", "red", "red"));
        //FindMostRepeated.find(Arrays.asList("a", "a", "a", "b", "b", "b"));
        //FindMostRepeated.find(Arrays.asList("a", "a", "a", "b", "b", "b"));
    }

    private static void find(final List<String> data) {
        final Map<String, Integer> countMap = data
            .stream()
            .collect(Collectors.toMap(key -> key, value -> 1, Integer::sum));

        countMap.entrySet()
            .stream()
            .max(Map.Entry.comparingByValue())
            .ifPresent(System.out::println);
    }

}
