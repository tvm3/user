package com.tvm.user.questions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortByCharacterLength {
    public static void main(final String[] args) {
        List<String> data = new ArrayList<>(List.of("aa", "aaaaaaabb", "ss", "aaaabbb", "aaa", "aaab"));
        System.out.println(sort(data));
    }

    private static List<String> sort(List<String> data) {
        Collections.sort(data, (s1, s2) -> {
            final int diff = countAs(s2) - countAs(s1);
            if (diff > 1) {
                return 1;
            } else if (diff < 1) {
                return -1;
            } else {
                return s2.length() - s1.length();
            }
        });
        return data;
    }

    private static int countAs(final String data) {
        int count = 0;
        if (data.isEmpty()) {
            return 0;
        }
        final char ch = data.charAt(0);
        if (ch == 'a') {
            count = count + 1;
        }
        return count + countAs(data.substring(1));
    }
}
