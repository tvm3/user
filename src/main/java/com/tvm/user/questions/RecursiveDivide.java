package com.tvm.user.questions;

public class RecursiveDivide {
    public static void main(final String[] args) {
        //RecursiveDivide.print(9);
        RecursiveDivide.print(2000);
        RecursiveDivide.print(0);
    }

    private static void print(final int num) {
        if (num <= 2) {
            System.out.println(num);
            return;
        }
        RecursiveDivide.print(num / 2);
        System.out.println(num);
    }
}
