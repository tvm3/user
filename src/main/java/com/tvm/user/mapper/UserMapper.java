package com.tvm.user.mapper;

import com.tvm.user.entity.UserEntity;
import com.tvm.user.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "id", ignore = true)
    UserEntity toEntity(User user);

    User toUser(UserEntity userEntity);

    List<User> toUsers(List<UserEntity> userEntities);

    List<UserEntity> toEntityList(List<User> users);
}
