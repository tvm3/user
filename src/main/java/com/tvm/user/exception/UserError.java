package com.tvm.user.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
class UserError {
    private String errorMessage;
}
