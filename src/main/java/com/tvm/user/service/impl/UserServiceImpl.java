package com.tvm.user.service.impl;

import com.tvm.user.entity.UserEntity;
import com.tvm.user.exception.UserNotFoundException;
import com.tvm.user.mapper.UserMapper;
import com.tvm.user.model.User;
import com.tvm.user.repository.UserRepository;
import com.tvm.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(final UserRepository userRepository, final UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public List<User> getUsers() {
        return this.userMapper.toUsers(this.userRepository.findAll());
    }

    @Override
    public User getUser(final Long id) {
        final UserEntity userEntity = this.userRepository.findById(id)
            .orElseThrow(() -> new UserNotFoundException("User does not exist " + id));
        return this.userMapper.toUser(userEntity);
    }

    @Override
    @Transactional
    public User updateUser(final User user) {
        //For complex and deeply nested objects. It's better to delete and recreate.
        //Of course the sequence or primary key will change but its good practice to not expose primary key to end user.
        //Use friendly unique ids (email or username)
        this.deleteUser(user.getId());
        return this.userMapper.toUser(this.userRepository.save(this.userMapper.toEntity(user)));
    }

    @Override
    @Modifying
    @Transactional
    public void deleteUser(final Long id) {
        this.getUser(id);
        this.userRepository.deleteById(id);
        this.userRepository.flush();
    }

    @Override
    @Transactional
    public User addUser(final User user) {
        return this.userMapper.toUser(this.userRepository.save(this.userMapper.toEntity(user)));
    }
}
