package com.tvm.user.service;

import com.tvm.user.model.User;

import java.util.List;

public interface UserService {
    List<User> getUsers();

    User getUser(Long id);

    User updateUser(User user);

    void deleteUser(Long id);

    User addUser(User user);
}
