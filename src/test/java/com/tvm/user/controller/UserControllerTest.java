package com.tvm.user.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@Sql("classpath:data.sql")
@Sql(scripts = "classpath:delete.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UserControllerTest {
    private static final String USERS_RESPONSE = "[{\"id\":1,\"firstName\":\"A\",\"lastName\":\"B\",\"email\":\"a@someting.com\"},{\"id\":2,\"firstName\":\"C\",\"lastName\":\"D\",\"email\":\"b@someting.com\"},{\"id\":3,\"firstName\":\"E\",\"lastName\":\"F\",\"email\":\"c@someting.com\"}]";
    private static final String USER_RESPONSE = "{\"id\":1,\"firstName\":\"A\",\"lastName\":\"B\",\"email\":\"a@someting.com\"}";
    private static final String UPDATE_USER = "{\"id\":1,\"firstName\":\"someothername\",\"lastName\":\"B\",\"email\":\"a@someting.com\"}";
    private static final String ADD_USER = "{\"firstName\":\"X\",\"lastName\":\"Y\",\"email\":\"x@someting.com\"}";
    private static final String UPDATE_USER_RESPONSE = "{\"id\":1,\"firstName\":\"someothername\",\"lastName\":\"B\",\"email\":\"a@someting.com\"}";
    @Autowired
    private MockMvc mockMvc;


    @Test
    public void getUsers() throws Exception {
        this.mockMvc.perform(get("/users"))
            .andDo(print()).andExpect(status().isOk())
            .andExpect(content().string(USERS_RESPONSE));
    }

    @Test
    public void getUser() throws Exception {
        this.mockMvc.perform(get("/users/1"))
            .andDo(print()).andExpect(status().isOk())
            .andExpect(content().string(USER_RESPONSE));
    }

    @Test
    @Sql("classpath:seq.sql")
    public void addUser() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(ADD_USER))
            .andExpect(status().is(200));
    }

    @Test
    public void updateUser() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders.put("/users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(UPDATE_USER))
            .andExpect(status().is(200))
            .andDo(print())
            .andExpect(content().string(UPDATE_USER_RESPONSE));
    }

    @Test
    public void userNotFound() throws Exception {
        this.mockMvc.perform(get("/users/10"))
            .andDo(print()).andExpect(status().is(HttpStatus.NOT_FOUND.value()));
    }

    @Test
    public void deleteUser() throws Exception {
        this.mockMvc.perform(delete("/users/1"))
            .andDo(print()).andExpect(status().isOk());
    }
}
